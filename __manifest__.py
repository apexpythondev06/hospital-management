{
    'name': 'Hospital MGMT',
    'version': '14.0.0.0',
    'category': 'Extra Tools',
    'summary': 'Hospital Management',
    'author': 'Apextechnomatics',
    'sequence': '12',
    'depends': ['base'],
    'demo': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/sequence.xml',
        'views/patient.xml',
        'views/doctor.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

