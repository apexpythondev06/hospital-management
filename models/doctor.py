# -*- coding: utf-8 -*-

from odoo import models, fields



class HospitalDoctor(models.Model):
    _name = 'doctor'
    _description = "Doctor's Record"

    name = fields.Char(string="Name", required=True)
    gender = fields.Selection([
        ('male', 'Male'),
        ('fe_male', 'Female'),
    ], default='male', string="Gender")
    user_id = fields.Many2one('res.User', string="user")

