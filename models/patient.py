# -*- coding: utf-8 -*-

from odoo import models, fields, api,  _
from odoo.exceptions import ValidationError

class HospitalPatient(models.Model):
    _name = 'patient'

    _description = 'Patient Record'
    _rec_name = 'name'



    def name_get(self):
        # name get function for the model executes automatically
        res = []
        for rec in self:
            res.append((rec.id, '%s - %s' % (rec.name_seq, rec.patient_name)))
        return res


    @api.constrains('patient_age')
    def check_age(self):
        for rec in self:
            if rec.patient_age < 5:
                raise ValidationError(_('Patient Age Must be Greater Than 5..!'))





    @api.model
    def create(self, vals):
        if vals.get('name_seq', _('New')) == _('New'):
            vals['name_seq'] = self.env['ir.sequence'].next_by_code('patient.sequence') or _('New')
        result = super(HospitalPatient, self).create(vals)
        return result


    name = fields.Char(string="Contact Number")
    name_seq = fields.Char(string='Patient ID', required=True, copy=False, readonly=True,
                           index=True, default=lambda self: _('New'))
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
    ], default='male', string="Gender")
    patient_name = fields.Char(string='Name', required=True,  track_visibility="always")
    patient_age = fields.Integer('Age', track_visibility="always", group_operator=False)
    image = fields.Binary(string="Image", attachment=True)
    active = fields.Boolean("Active", default=True)
    doctor_id = fields.Many2one('doctor', string='Doctor')
    email_id = fields.Text(string='email')




